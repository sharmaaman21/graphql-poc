var {Sequelize, Model, DataTypes} = require('sequelize');
const { Client } = require('pg');
var _ = require('lodash');
var Faker  = require('faker');
const { v4: uuidv4 } = require('uuid');

const user = 'postgresadmin'
const host = 'localhost'
const database = 'empdetails'
const password = 'aman'
const port = '5432'

const conn = new Sequelize(database, user, password, {
    host,
    port,
    dialect: 'postgres',
    logging: false
});

const Employee = conn.define('employee', {
    id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1,
        primaryKey: true
    },
    firstName: {
        type: DataTypes.STRING
    },
    lastName: {
        type: DataTypes.STRING
    },
    email: {
        type: DataTypes.STRING
    },
    gender: {
        type: DataTypes.ENUM(['Male', 'Female', 'Other'])
    },
    experience: {
        type: DataTypes.INTEGER
    },
    techLanguages: {
        type: DataTypes.STRING, 
        get: function() {
            return JSON.parse(this.getDataValue('techLanguages'));
        }, 
        set: function(val) {
            return this.setDataValue('techLanguages', JSON.stringify(val));
        }
    }
});

const dummyLanguages = [
    { langName: "Graphql", version: "1.0.0"},
    { langName: "Node", version: "1.0.0"},
    { langName: "Angular", version: "1.0.0"},
    { langName: "Postgres", version: "1.0.0"}
]

conn.sync({ force: true }).then(()=> {
    _.times(5, ()=> {
      return Employee.create({
        id: uuidv4(),
        firstName: Faker.name.firstName(),
        lastName: Faker.name.lastName(),
        email: Faker.internet.email(),
        gender: 'Male',
        experience: 4,
        techLanguages: dummyLanguages
      })
    });
  });

module.exports = conn;
