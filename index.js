var express = require('express');
var { graphqlHTTP } = require('express-graphql');
var { buildSchema } = require('graphql');
var schemas = require('./schema');
var resolvers = require('./resolvers');
 
// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hey: String
    hello: String
  }
`);
 
// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
  hey: () => {
    return 'I am great';
  },
};

var emp = resolvers;
 
var app = express();
app.get('/', (req, res) => {
    res.send("Up and running with graphql.");
});

app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));

app.use('/empDetails', graphqlHTTP({
    schema: schemas,
    rootValue: emp,
    graphiql: true,
  }));

app.listen(8081); //"predev":"fuser -k 8081/tcp && echo 'Terminated' || echo 'Nothing was running on the PORT'",
console.log('Running a GraphQL API server at http://localhost:8081');
