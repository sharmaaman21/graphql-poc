var {graphql, buildSchema } = require('graphql');
var Db = require('./db');

const schema = buildSchema(`
    type EmpDetails {
        id: ID
        firstName: String
        lastName: String
        gender: gender
        email: String
        experience: Int
        techLanguages: [techLanguages]
    }

    type techLanguages {
        langName: String
        version: String
    }

    enum gender {
        Male
        Female
        Other
    }

    type Query {
        getEmpDetails(id: ID): EmpDetails
    }

    input empDetailsInput {
        id: ID
        firstName: String!
        lastName: String!
        gender: gender!
        email: String!
        experience: Int!
        techLanguages: [techLanguagesInput]!
    }

    input techLanguagesInput {
        langName: String
        version: String
    }

    type Mutation {
        createEmpDetails(input: empDetailsInput): EmpDetails
    } 
`); 

/**
 * Query is responsible for getting the data from database.
 * 
 * Mutation is responsible for feeding data to database.
 */

module.exports = schema;
