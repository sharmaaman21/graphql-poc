**1. First clone the repo.**
**2. Open Terminal and paste this command *npm install*.**
**3. Now run the command *npm start***

**Mutation: (Seeding data to database)**

mutation {
  createEmpDetails(input: {firstName: "Aman", lastName: "Sharma", gender: Male, email: "amansharma21121994@gmail.com", experience: 4, techLanguages: [{langName: "MEAN", version: "1.0.0"}, {langName: "JAVA", version: "1.0.0"}]}) {
    id
    firstName
    lastName
  }
}

**Query: (Getting data to database)**
query{
  getEmpDetails(id: "0f0a3c22-640c-4b46-aa17-27405cb6e1de"){
    id
    techLanguages{
      langName
    }
  }
}
