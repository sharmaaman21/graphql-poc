const { v4: uuidv4 } = require('uuid');
var Db = require('./db');

class EmpDetails {
    constructor(id, {firstName, lastName, gender, email, experience, techLanguages}){
        this.id = id
        this.firstName = firstName
        this.lastName = lastName
        this.gender = gender
        this.email = email
        this.experience = experience
        this.techLanguages = techLanguages
    }
}

const details = {}

const resolvers = {
    getEmpDetails: ({id}) => {
        return Db.models.employee.findByPk(id);
        // return new EmpDetails(id, details[id])
    },
    createEmpDetails: ({input}) => {
        let id = uuidv4()
        details[id] = input
        console.log(input.techLanguages);
        return Db.models.employee.create({
            id: id,
            firstName: input.firstName,
            lastName: input.lastName,
            gender: input.gender,
            experience: input.experience,
            email: input.email,
            techLanguages: input.techLanguages,
        })
        // return new EmpDetails(id, input)
    }
}

module.exports = resolvers;
